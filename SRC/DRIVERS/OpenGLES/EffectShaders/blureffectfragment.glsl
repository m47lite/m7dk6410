//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//============================================================================

precision highp float;

const int MaxRadius = 14;

uniform    sampler2D   inTexture;
uniform    bool        colorConversion;
uniform    int         inRadius;
uniform    int         width;
uniform    int         height;
uniform    float       gaussianTable[(MaxRadius+1) * (MaxRadius+1)];

varying    vec2        textureCoordinate;
varying    vec4        diffuse;


//============================================================================
//  GetGaussian()
//============================================================================
//
// Gets the gaussian value from the gaussian table and multiplies it with the offset value of the texture
//
//============================================================================
vec4 GetGaussian(sampler2D inTexture, vec2 textureCoordinate, int idxX, int idxY, int radius)
{
    int indexX = idxX >= 0 ? idxX : - idxX;
    int indexY = idxY >= 0 ? idxY : - idxY;
    float gaussianValue = gaussianTable[radius * indexX + indexY];
    
    float XOffset = float(idxX) / float(width);
    float YOffset = float(idxY) / float(height);
    float XCoordinate = textureCoordinate.x + XOffset;
    float YCoordinate = textureCoordinate.y + YOffset;
    XCoordinate = min(max(XCoordinate, 0.0), 1.0);
    YCoordinate = min(max(YCoordinate, 0.0), 1.0);
    return gaussianValue * texture2D(inTexture, vec2(XCoordinate, YCoordinate));
    
}// GetGaussian


//============================================================================
//  main()
//============================================================================
//
// Main function
//
//============================================================================
void main(void)
{
    int radius = 5;
    if (inRadius >= 0 && inRadius <= MaxRadius)
        radius = inRadius;
    
   // Calculate all the gaussian values
   vec4 total = vec4(0, 0, 0, 0);
   for (int idxX = -radius; idxX <= radius; ++idxX)
       for (int idxY = -radius; idxY <= radius; ++idxY)
           total += GetGaussian(inTexture, textureCoordinate, idxX, idxY, radius);
           
   vec4 color = diffuse * total;

   // This is worth removing if you know your hardware does BGRA;
   // even the if() test can be expensive.
   if (colorConversion)
   {
       // Do BGRA -> RGBA color conversion
       float temp  = color.r;
       color.r     = color.b;
       color.b     = temp;
   }

   gl_FragColor = color;
   
}// main

