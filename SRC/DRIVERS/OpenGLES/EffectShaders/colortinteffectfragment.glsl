//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//============================================================================

precision highp float;

uniform    sampler2D   inTexture1;
uniform    bool        colorConversion;
uniform    vec3        colorMask;

varying    vec2        textureCoordinate;
varying    vec4        diffuse;


//============================================================================
//  main()
//============================================================================
//
// Main function
//
//============================================================================
void main(void)
{
   vec4 color = diffuse * texture2D(inTexture1, textureCoordinate);

   // Make the item colored according to color mask
   float total = color.r + color.g + color.b;
   color.r = total * colorMask.r / 3.0;
   color.g = total * colorMask.g / 3.0;
   color.b = total * colorMask.b / 3.0;

   // This is worth removing if you know your hardware does BGRA;
   // even the if() test can be expensive.
   if (colorConversion)
   {
       // Do BGRA -> RGBA color conversion
       float temp  = color.r;
       color.r     = color.b;
       color.b     = temp;
   }
   
   gl_FragColor = color;
   
}// main

